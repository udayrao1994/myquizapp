import React, { Component } from "react";

import quiz from "./quiz";

import http from "./httpService.js";

class JslMain extends Component {
  state = {
    detail: [
      {
        text: "What is the capital of India",
        options: ["New Delhi", "London", "Paris", "Tokyo"],
        answer: 0,
        answered: -1,
      },
      {
        text: " What is the capital of Italy",
        options: ["Berlin", "London", "Rome", "Paris"],
        answer: 2,
        answered: -1,
      },
      {
        text: " What is the capital of China",
        options: ["Shanghai", "HongKong", "Shenzen", "Beijing"],
        answer: 3,
        answered: -1,
      },
      {
        text: " What is the capital of Nepal",
        options: ["Tibet", "Kathmandu", "Colombo", "Kabul"],
        answer: 2,
        answered: -1,
      },

      {
        text: " What is the capital of Bangladesh",
        options: ["Teheran", "Rome", "Maldives", "Dhaka"],
        answer: 2,
        answered: -1,
      },

      {
        text: " What is the capital of Sri Lanka",
        options: ["Islamabad", "Colombo", "Maldives", "Dhaka"],
        answer: 1,
        answered: -1,
      },

      {
        text: " What is the capital of France",
        options: ["London", "New York", "Paris", "Rome"],
        answer: 2,
        answered: -1,
      },
      {
        text: " What is the capital of Italy",
        options: ["Berlin", "London", "Paris", "Rome"],
        answer: 0,
        answered: -1,
      },
      {
        text: " What is the capital of Sweden",
        options: ["Helsinki", "Stockholm", "Copenhagen", "Lisbon"],
        answer: 1,
        answered: -1,
      },
      {
        text: " What is the currency of UK",
        options: ["Dollar", "Mark", "Yen", "Pound"],
        answer: 3,
        answered: -1,
      },
      {
        text: " What is the height of Mount Everest",
        options: ["9231 m", "8849 m", "8027 m", "8912 m"],
        answer: 1,
        answered: -1,
      },
      {
        text: " What is the capital of Japan",
        options: ["Beijing", "Kyoto", "Tokyo"],
        answer: 2,
        answered: -1,
      },

      {
        text: " What is 8 * 9",
        options: ["72", "76", "64", "81"],
        answer: 0,
        answered: -1,
      },

      {
        text: "What is 2*2",
        options: ["2", "4", "8", "10"],
        answer: 1,
        answered: -1,
      },
      {
        text: " What is 3*3",
        options: ["9", "12", "19", "15"],
        answer: 0,
        answered: -1,
      },
      {
        text: " What is 4*5",
        options: ["20", "30", "40", "50"],
        answer: 0,
        answered: -1,
      },
      {
        text: " What is 10+10+10",
        options: ["19", "15", "90", "30"],
        answer: 3,
        answered: -1,
      },

      {
        text: " What is 100*2",
        options: ["100", "150", "200", "400"],
        answer: 2,
        answered: -1,
      },
      {
        text: " What is 2*3+4*5",
        options: ["70", "50", "26", "60"],
        answer: 2,
        answered: -1,
      },
    ],

    questionsSet: [],
    view: 0,
    currentIndex: 0,
    reload: true,
    errorcount: 0,
    playername: "arvind",
    retrycount: 0,
    details: [],
  };

  next = (index) => {
    let s1 = { ...this.state };

    s1.reload = false;

    let currentquestion = s1.questionsSet[index - 1];
    if (currentquestion.answer == currentquestion.answered) {
      if (index > -1) s1.currentIndex = index;
    } else {
      s1.errorcount++;
      console.log("error", s1.errorcount);
      alert("Please choose correct answer.Your answer is wrong");
    }

    this.setState(s1);
  };

  resetquestion = (s1) => {
    if (s1.reload == true) {
      s1.questionsSet = [];
      for (let i = 0; i < 5; i++) {
        let x = Math.floor(Math.random() * 19 + 0);
        let question = s1.detail[x];

        s1.questionsSet.push(question);
      }
    }

    s1.detail.map((ele) => (ele.answered = -1));
    this.setState(s1);
  };

  componentWillMount() {
    let s1 = { ...this.state };
    this.resetquestion(s1);
  }

  addanswrered = (optindex) => {
    let s1 = { ...this.state };

    let obj = s1.questionsSet[s1.currentIndex];

    obj.answered = optindex;

    this.setState(s1);
  };

  Finish = () => {
    let s1 = { ...this.state };

    let data = {
      errorcount: s1.errorcount,
      playername: s1.playername,
      retrycount: s1.retrycount,
    };
    this.postData("/quizData", data);

    this.showresult();
    s1.view = 1;
    this.setState(s1);
  };

  async showresult() {
    let response = await http.get(`/quizData`);
    console.log(response);

    let { data } = response;

    let { details } = this.state;
    details = data;

    this.setState({ details: data });
    console.log("details", details);
  }
  async postData(url, obj) {
    console.log("url", url);
    let response = await http.post(url, obj);

    console.log("response", response);
  }
  onRetry = () => {
    let s1 = { ...this.state };
    s1.reload = true;
    s1.currentIndex = 0;
    s1.retrycount++;
    console.log("s1.retry", s1.retrycount);
    this.resetquestion(s1);
  };
  previous = (index) => {
    let s1 = { ...this.state };
    if (index <= s1.detail.length - 1) s1.currentIndex = index;

    this.setState(s1);
  };

  render() {
    let { details, currentIndex, questionsSet, view, optindex } = this.state;

    console.log("view", view);

    let currentdetail = questionsSet[currentIndex];
    return view === 0 ? (
      <div className="container">
        <h4 className="text-center">Welcome to Quiz App</h4>

        <quiz
          details={currentdetail}
          index={currentIndex}
          next={this.next}
          addans={this.addanswrered}
          onPrevious={this.previous}
          onRetry={this.onRetry}
          onFinish={this.Finish}
        />
      </div>
    ) : (
      details.map((ele) => (
        <React.Fragment>
          <div className="container border">
            <div className="row">
              <div className="col-2">PlayerName: {ele.playername}</div>
              <div className="col-2">ErorCount: {ele.errorcount}</div>
              <div className="col-2">RetryCount : {ele.retrycount}</div>
            </div>
          </div>
        </React.Fragment>
      ))
    );
  }
}

export default JslMain;
