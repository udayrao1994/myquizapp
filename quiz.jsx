

import React, { Component } from "react";


class quiz extends Component {
    state = {
      
      selectedOption:''

    }

    handleChange=(e)=>
    {
        let {currentTarget:input}=e;
        
        let selectedindex=this.props.details.options.findIndex((ele)=>ele==input.value)
       
        this.props.addans(selectedindex);
        
    }
    
    render() {
    
    
        let {details,next,addans,onRetry,onFinish,index} = this.props;
       
        return (
           <div className="container">
               <h5>{this.props.edit?"Edit deatails":""}</h5>

               

              
            
               <h5 >Questions</h5>
                
               <div className="row">   
                     <div className="col">

                        
                      <h6> {details.text}</h6> 
                        
                        {details.options.map((ele,optindex)=>(
                            
                         <div>
                        
                        <input 
                        className="form-check-input "

                        value={ele}
                        onChange={this.handleChange}
                        type="radio"
                        name="opt"
                        checked={details.options[details.answered]==ele}
                        />

<label className="form-check-label ">{ele} </label>


                             
                             <br/></div>
                         ) )}
                      
                      {"Your Answer : " + (details.answered == -1 ? 
                      " Not answered" :details.answered == 0 ? 
                      "A":details.answered == 1 ?"B":details.answered == 2 ?"C":details.answered == 3 ?"D":"")}

                      
             </div>
             
             

        <div><br/></div>
        </div>
        <br></br>
        
     
       
       {(index==4)?
       
           <React.Fragment>
           
           
           
               
           <button className="btn btn-primary m-1 "  onClick={()=>onFinish(index)} >Finish</button>
           


           
     <button className="btn btn-primary m-1 "  onClick={()=>onRetry()} > Retry</button>
     </React.Fragment>
       

       :<div > <button className="btn btn-primary "  onClick={()=>next(index+1)} >Next Question</button></div>
        }
       </div>

           
        
        );
      
                  
    }
            
            }
export default quiz;